package com.vehiculum.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class hotOffer {

	private WebDriver driver;
	String URL = "https://www.vehiculum.de";

	@BeforeClass
	public void testSetUp() {

		driver = new ChromeDriver();
	}

	@Test(priority = 1)
	public void accessVehiculum() throws InterruptedException {

		System.out.println("Navigating to Vehiculum");

		driver.get(URL);
		Thread.sleep(1000);
	}
	
	@Test(priority = 2)
	public void topDeals() throws InterruptedException {
		
		System.out.println("Checking Top Deals");
		
		String firstVehicle = driver.findElement(By.xpath("//*[@class='h2-car-link' and @data-index='1']")).getAttribute("data-car-model-name");
		
		System.out.println("Vehicle 1 is: " + firstVehicle);
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath("//*[@class='h2-car-link' and @data-index='1']")));
		Thread.sleep(2000);
		String vehicleshown1 = driver.findElement(By.xpath("//*[@class='text-primary']")).getText();
		System.out.println("Vehicle shown when clicked is: " + vehicleshown1);
		driver.navigate().back();
		
		String secondVehicle = driver.findElement(By.xpath("//*[@class='h2-car-link' and @data-index='2']")).getAttribute("data-car-model-name");
		
		System.out.println("Vehicle 2 is: " + secondVehicle);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath("//*[@class='h2-car-link' and @data-index='2']")));
		Thread.sleep(2000);
		String vehicleshown2 = driver.findElement(By.xpath("//*[@class='text-primary']")).getText();
		System.out.println("Vehicle shown when clicked is: " + vehicleshown2);
		driver.navigate().back();
		
		String thirdVehicle = driver.findElement(By.xpath("//*[@class='h2-car-link' and @data-index='3']")).getAttribute("data-car-model-name");
		
		System.out.println("Vehicle 3 is: " + thirdVehicle);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath("//*[@class='h2-car-link' and @data-index='3']")));
		Thread.sleep(2000);
		String vehicleshown3 = driver.findElement(By.xpath("//*[@class='text-primary']")).getText();
		System.out.println("Vehicle shown when clicked is: " + vehicleshown3);
		driver.navigate().back();
		
		System.out.println("Closing the Window");
		driver.close();
		
	}
	
	@AfterClass
	public void tearDown() {
			
			driver.quit();
		}
}
