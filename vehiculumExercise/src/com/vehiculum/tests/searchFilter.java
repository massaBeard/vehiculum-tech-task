package com.vehiculum.tests;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class searchFilter {

	private WebDriver driver;
	String URL = "https://www.vehiculum.de";

	@BeforeClass
	public void testSetUp() {

		driver = new ChromeDriver();
	}

	@Test(priority = 1)
	public void accessVehiculum() throws InterruptedException {

		System.out.println("Navigating to Vehiculum");

		driver.get(URL);
		Thread.sleep(1000);
	}

	@Test(priority = 2)
	public void accessfirstDropdown() throws InterruptedException {

		System.out.println("Choosing 3 manufacturer, Jaguar - Audi - Jeep");

		driver.findElement(By.xpath("//button[@class='btn h2-hero-banner-header-form-btn dropdown-toggle']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id = 'manufacturer_Jaguar']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//input[@id = 'manufacturer_Audi']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//input[@id = 'manufacturer_Jeep']")).click();
		Thread.sleep(500);

	}

	@Test(priority = 3)
	public void accesssecondDropdown() throws InterruptedException {

		System.out.println("Choosing Vehicle Type");

		driver.findElement(By.xpath("//button[@id='select-body-type']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//input[@id='body_type_suv']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//input[@id='body_type_sports']")).click();
	}

	@Test(priority = 4)
	public void accessthirdDropdown() throws InterruptedException {

		System.out.println("Choosing Maximum Finance Rate - 15000");

		driver.findElement(By.xpath("//button[@id='select-budget']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//input[@id='finance_rate_to']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//input[@id='finance_rate_to']")).sendKeys("15000");

		System.out.println("Submitting Selections");
		driver.findElement(By.xpath("//input[@id='homepage-find-car']")).click();
		Thread.sleep(1000);
	}

	@Test(priority = 5)
	public void verifyResults() throws InterruptedException {
		
	
		System.out.println("Verifying Vehicle Type");
		
		Assert.assertEquals(driver.findElement(By.xpath("//input[@id='manufacturer_jaguar']")).isSelected(), true);
		Assert.assertEquals(driver.findElement(By.xpath("//input[@id='manufacturer_jeep']")).isSelected(), true);
		Assert.assertEquals(driver.findElement(By.xpath("//input[@id='manufacturer_audi']")).isSelected(), true);
		
		System.out.println("Verifying Body Type");
		
		Assert.assertEquals(driver.findElement(By.xpath("//input[@id='body_type_5']")).isSelected(), true);
		Assert.assertEquals(driver.findElement(By.xpath("//input[@id='body_type_10']")).isSelected(), true);
		
		System.out.println("Verifying Budget");
		
		String maxBudget = driver.findElement(By.xpath("//input[@name='finance_rate_to']")).getAttribute("value");
		
		Assert.assertEquals(maxBudget,"15000");
		
		System.out.println("Closing the Window");
		driver.close();
	}
	
	@AfterClass
	public void tearDown() {
			
			driver.quit();
		}
	}