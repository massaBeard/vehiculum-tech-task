package com.vehiculum.tests;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class logoCheck {

	private WebDriver driver;
	String URL = "https://www.vehiculum.de";

	@BeforeClass
	public void testSetUp() {

		driver = new ChromeDriver();
	}

	@Test (priority=1)
	public void logoCollect() throws InterruptedException{
	
	driver.get(URL);
	
	Map<Integer, List<String>> map = driver.findElements(By.xpath("//*[@class='zoom-parent col-xs-6 col-sm-4 align-center outline-none h2-car-brand']")) 
            .stream()                             // find all elements which has href attribute & process one by one
            .map(ele -> ele.getAttribute("href")) // get the value of href
            .distinct()                           // there could be duplicate links , so find unique
            .collect(Collectors.groupingBy(LinkUtil::getResponseCode)); // group the links based on the response code
            
	map.get(200) // will contain all the good urls
	.forEach(System.out::println);
	map.get(403); // will contain all the 'Forbidden' urls
	map.get(404); // will contain all the 'Not Found' urls 
	map.get(0); // will contain all the unknown host urls
	
	
	
	driver.findElement(By.xpath("//span[contains(text(), 'Weitere Marken')]")).click();
	Thread.sleep(2000);
	String Models = driver.findElement(By.xpath("//span[@class= 'vc-total-count']")).getText();
	
	System.out.println("There are " + Models + " models of vehicles on sale.");
	Thread.sleep(1000);
	
	System.out.println("Closing the Window");
	driver.close();
	
	}
	
	public static class LinkUtil {

		// hits the given url and returns the HTTP response code
		public static int getResponseCode(String link) {
			java.net.URL url;
			HttpURLConnection con = null;
			Integer responsecode = 0;
			try {
				url = new java.net.URL(link);
				con = (HttpURLConnection) url.openConnection();
				responsecode = con.getResponseCode();
			} catch (Exception e) {
				// skip
			} finally {
				if (null != con)
					con.disconnect();
			}
			return responsecode;
		}

	}

	@AfterClass
	public void tearDown() {
			
			driver.quit();
		}

}
